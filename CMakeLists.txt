cmake_minimum_required(VERSION 2.8)
project(DBoW2)

if (CMAKE_BUILD_TYPE EQUAL "Release")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -Wall  -O3 -march=native ")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall  -O3 -march=native")
endif()

set(HDRS_DBOW2
  include/DBoW2/BowVector.h
  include/DBoW2/FORB.h
  include/DBoW2/FClass.h
  include/DBoW2/FeatureVector.h
  include/DBoW2/ScoringObject.h
  include/DBoW2/TemplatedVocabulary.h)
set(SRCS_DBOW2
  ${HDRS_DBOW2}
  DBoW2/BowVector.cpp
  DBoW2/FORB.cpp
  DBoW2/FeatureVector.cpp
  DBoW2/ScoringObject.cpp)

set(HDRS_DUTILS
  include/DUtils/Random.h
  include/DUtils/Timestamp.h)
set(SRCS_DUTILS
  ${HDRS_DUTILS}
  DUtils/Random.cpp
  DUtils/Timestamp.cpp)

find_package(OpenCV 3.0 QUIET)
if(NOT OpenCV_FOUND)
   find_package(OpenCV 2.4.3 QUIET)
   if(NOT OpenCV_FOUND)
      message(FATAL_ERROR "OpenCV > 2.4.3 not found.")
   endif()
endif()
find_package(TBB REQUIRED)

include_directories(${OpenCV_INCLUDE_DIRS})
add_library(DBoW2 ${SRCS_DBOW2} ${SRCS_DUTILS})
target_include_directories(DBoW2 PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:include>)
target_link_libraries(DBoW2 ${TBB_LIBRARIES} ${OpenCV_LIBS})

